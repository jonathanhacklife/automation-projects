# Selenium Webdriver

## Introducción

WebDriver maneja un navegador de forma nativa, como lo haría un usuario, ya sea localmente o en una máquina remota usando el servidor Selenium, marca un salto adelante en términos de automatización del navegador.

Selenium WebDriver se refiere tanto a los enlaces de idioma como a las implementaciones del código de control del navegador individual. Esto se conoce comúnmente como simplemente *WebDriver* .

Selenium WebDriver es un [Recomendación del W3C](https://www.w3.org/TR/webdriver1/)

- WebDriver está diseñado como una interfaz de programación simple y más concisa.
- WebDriver es una API compacta orientada a objetos.
- Maneja el navegador de manera efectiva.

## Instalación

[Installing Selenium libraries](https://www.selenium.dev/documentation/en/selenium_installation/installing_selenium_libraries/)

## Manipulación del navegador

[Browser manipulation](https://www.selenium.dev/documentation/en/webdriver/browser_manipulation/)

## Localización de elementos

[Locating elements](https://www.selenium.dev/documentation/en/webdriver/locating_elements/)

## Elemento web

[Web element](https://www.selenium.dev/documentation/en/webdriver/web_element/)

## Obtener el XPATH de un elemento

[XPath in Selenium WebDriver Tutorial: How to Find XPath?](https://www.guru99.com/xpath-selenium.html)

## Teclado

[Keyboard](https://www.selenium.dev/documentation/en/webdriver/keyboard/)