# PASAR ARCHIVO ".JSON" A PDF
# https://csvjson.com/csv2json
from fpdf import FPDF
import os
import json

def crearPDF(archivo, esSabana, cantidad, propiedad):
    pdf = FPDF()
    total_lines = 0
    data = ""

    # Abrir archivo
    with open(archivo) as myfile:
        data = json.load(myfile)

    # Obtener total de líneas
    if (cantidad):
        total_lines = cantidad
    else:
        total_lines = sum(1 for line in data)

    # Asignar nombre de carpeta
    if esSabana:
        dirName = "Sabanas"
    else:
        dirName = str(total_lines) + " docs"

    # Crear carpeta
    parent_dir = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(parent_dir, dirName)
    print(f"Creando carpeta {dirName} en {parent_dir}")
    os.mkdir(path)

    aux = 0

    for item in data:
        aux = aux + 1
        if aux <= total_lines:
            # CREA ARCHIVO TXT
            miarchivo = open(
                f"{dirName}/{item[propiedad]}.txt", "a+")
            concatenate = ''
            for key, value in item.items():
                concatenate += "{key}: {value}\n".format(key=key, value=value)
            miarchivo.write(f"{concatenate}")
            miarchivo.close()
            pdf.add_page()  # Agregar una página
            pdf.set_font("Arial", size=8)  # Establecer tipografía y tamaño
            # pdf.image('sampleImage.jpg', 10, 20, 33) # Agregar una imagen
            line = 1

            # LECTURA ".txt"/ESCRITURA ".pdf"
            # ABRIR FICHERO A CONVERTIR
            # acá toma el archivo entero.
            archivoTexto = open(f"{dirName}/{item[propiedad]}.txt", "r")

            # Por cada línea del archivo, imprime en el PDF
            for linea in archivoTexto:
                pdf.cell(200, 7, txt=linea, ln=line, align="L")
                if linea[-1] == ("\n"):
                    linea = linea[:-1]
                line += 1

            # SALIDA PDF
            # Acá le digo que los escupa como .pdf
            if esSabana == False:
                pdf.output(f"{dirName}/{item[propiedad]}.pdf")
            print(f"📑 Creando PDF: {item[propiedad]}.PDF")

            # CERRAR FICHERO.
            archivoTexto.close()
    if esSabana:
        pdf.output(f"{dirName}/Sabana{total_lines}.pdf")
        print(f"📑 Creando Sabana de {total_lines} documentos...")
    eliminarAuxiliares(archivo, esSabana, total_lines, propiedad)

def eliminarAuxiliares(archivo, esSabana, cantidad, propiedad ):
    aux = 0
    total_lines = 0
    if (cantidad):
        total_lines = cantidad
    if esSabana:
        dirName = "Sabanas"
    else:
        dirName = str(total_lines) + " docs"
    with open(archivo) as myfile:
        data = json.load(myfile)
        for item in data:
            aux = aux + 1
            if aux <= total_lines:
                print(
                    f"🔥 Eliminando archivo: {item[propiedad]}.txt")
                os.remove(f"{dirName}/{item[propiedad]}.txt")


crearPDF("csvjson.json", True, False, "dni")
