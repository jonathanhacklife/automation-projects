﻿# PASAR ARCHIVO ".txt" A PDF
from fpdf import FPDF
import os



def crearPDF(archivo, cantidad):
    with open(archivo) as myfile:
        total_lines = sum(1 for line in myfile)

    if (cantidad):
        total_lines = cantidad
    dirName = str(total_lines) + "docs"
    parent_dir = os.path.dirname(os.path.abspath(__file__))
    print(f"Creando carpeta {dirName} en {parent_dir}")

    path = os.path.join(parent_dir, dirName)
    os.mkdir(path)

    aux = 0
    for dni in open(archivo):
        aux = aux + 1
        if aux <= total_lines:
            dni = dni.rstrip("\n")
            miarchivo = open(f"{dirName}/{dni}.txt", "a+")
            miarchivo.write(f"DNI {dni}")  # DNI:1234567890
            miarchivo.close()

            # OBJETO "pdf"
            pdf = FPDF()
            pdf.add_page()  # Agregar una página
            pdf.set_font("Arial", size=10)  # Establecer tipografía y tamaño
            # pdf.image('sampleImage.jpg', 10, 20, 33) # Agregar una imagen
            line = 1

            # LECTURA ".txt"/ESCRITURA ".pdf"
            # ABRIR FICHERO A CONVERTIR
            # acá toma el archivo entero.
            archivoTexto = open(f"{dirName}/{dni}.txt", "r")

            # Por cada línea del archivo, imprime en el PDF
            for linea in archivoTexto:
                pdf.cell(200, 7, txt=linea, ln=line, align="L")
                if linea[-1] == ("\n"):
                    linea = linea[:-1]
                line += 1

            # SALIDA PDF
            # Acá le digo que los escupa como .pdf
            pdf.output(f"{dirName}/{dni}.pdf").encode('latin-1', 'ignore')
            print(f"📑 Creando PDF: {dni}.PDF")

            # CERRAR FICHERO.
            archivoTexto.close()
    eliminarAuxiliares(archivo, cantidad)


def eliminarAuxiliares(archivo, cantidad):
    aux = 0
    if (cantidad):
        total_lines = cantidad
    dirName = str(total_lines) + "docs"
    for dni in open(archivo):
        aux = aux + 1
        if aux <= total_lines:
            dni = dni.rstrip("\n")
            print(f"🔥 Eliminando archivo: {dni}.txt")
            os.remove(f"{dirName}/{dni}.txt")


crearPDF("MOCK_DATA_1.txt", 100)
