﻿from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
import time
import sys

# Iniciar WebDriver
with Chrome() as driver:

    # Navegar a la URL
    driver.get("http://www.facebook.com")
    print(f"ABRIENDO [{driver.current_url}]...")

    # Obtener el valor textual del elemento
    descripcionWeb = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div[1]/div/div/div/div[1]/h2").get_attribute('innerHTML')
    print(f"DESCRIPCIÓN DE LA WEB: [{descripcionWeb}]")
    descripcionWeb == "Google"

    # Enfocar el input de usuario
    driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div[1]/div/div/div/div[2]/div/div[1]/form/div[1]/div[1]/input").send_keys("user email")
    print("CAMPO DE USUARIO [✔]")

    # Enfocar el input de contraseña
    driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div[1]/div/div/div/div[2]/div/div[1]/form/div[1]/div[2]/div/input").send_keys("password")
    print("CAMPO DE CONTRASEÑA [✔]")

    # Clickear el botón "Iniciar sesión"
    driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div[1]/div/div/div/div[2]/div/div[1]/form/div[2]/button").click()
    print("BOTÓN DE INICIAR SESIÓN [✔]")

    # Esperar 10 segundos antes de cerrar
    print("CERRANDO VENTANA EN:")
    for i in range(10, 0, -1):
        sys.stdout.write(str(i)+' ')
        sys.stdout.flush()
        time.sleep(1)

    # Cerrar WebDriver
    driver.quit()
    print("Ventana cerrada por el sistema")
